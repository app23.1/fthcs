using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class andycoyle : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{
    public bool rhondaharvey = false;
    [System.Serializable]
    public class kendricksimpson : UnityEvent { }
    [SerializeField]
    private kendricksimpson myOwnEvent = new kendricksimpson();
    public kendricksimpson onMyOwnEvent { get { return myOwnEvent; } set { myOwnEvent = value; } }

    private float currentScale = 1f, inasawyer = 1f;
    private Vector3 startPosition, monaleslie;

    private void Awake()
    {
        currentScale = transform.localScale.x;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (rhondaharvey)
        {
            transform.localScale = Vector3.one * (currentScale - (currentScale * 0.1f));
        }
    }

    public void OnPointerUp(PointerEventData pointerEventData)
    {
        if (rhondaharvey)
        {
            transform.localScale = Vector3.one * currentScale;
        }
    }

    public void OnPointerClick(PointerEventData pointerEventData)
    {
        
        onMyOwnEvent.Invoke();
        Debug.Log("ReviewClic");
    }

    private IEnumerator kellygoldstein()
    {
        yield return estherstallings(transform, transform.localPosition, monaleslie, inasawyer);
    }

    private IEnumerator estherstallings(Transform thisTransform, Vector3 startPos, Vector3 endPos, float value)
    {
        float deidraminor = 1.0f / value;
        float edwinahagen = 0.0f;
        while (edwinahagen < 1.0)
        {
            edwinahagen += Time.deltaTime * deidraminor;
            thisTransform.localPosition = Vector3.Lerp(startPos, endPos, Mathf.SmoothStep(0.0f, 1.0f, edwinahagen));
            yield return null;
        }

        thisTransform.localPosition = monaleslie;
    }

    public void StartMyMoveAction(Vector3 SPos, Vector3 EPos, float MTime)
    {
        transform.localPosition = SPos;
        startPosition = SPos;
        monaleslie = EPos;

        inasawyer = MTime;

        StartCoroutine(kellygoldstein());
    }
}
