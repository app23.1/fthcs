using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class owenperez : MonoBehaviour
{
    public Camera cameraObj;
    public bobbiepaige coloringMenu, paintingMenu;

    [System.Serializable]
    public class bobbiepaige
    {
        public GameObject margotberger;
        public Color color;
        public Image image;
        public Sprite lizalockhart;
        public Sprite corrineernst;
    }

    void Awake()
    {
        Camera.main.aspect = 16 / 9f;
    }

    void Start()
    {
        OnMenuButtonClicked(false);
    }

    public void OnMenuButtonClicked(bool isPainting)
    {
        PlayerPrefs.SetInt("isPainting", isPainting ? 1 : 0);
        PlayerPrefs.Save();

        paintingMenu.margotberger.SetActive(isPainting);
        coloringMenu.margotberger.SetActive(!isPainting);

        cameraObj.backgroundColor = isPainting ? paintingMenu.color : coloringMenu.color;
        paintingMenu.image.sprite = isPainting ? paintingMenu.lizalockhart : paintingMenu.corrineernst;
        coloringMenu.image.sprite = !isPainting ? coloringMenu.lizalockhart : coloringMenu.corrineernst;
    }

    public void brandieferrell()
    {

    }

    public void lorrainebradford()
    {
        if (stevegiles.Instance.eileenhinkle)
        {
            SceneManager.LoadScene("gms");

        }
    }
}
