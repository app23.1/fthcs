using UnityEngine;

public class nickolasthorpe : MonoBehaviour
{
    public AudioClip clickSound, cameraSound;

    public static nickolasthorpe USE;

    private AudioSource bessiecho;

    private void Awake()
    {
       
        if (USE == null)
        {
            USE = this;
            DontDestroyOnLoad(gameObject);

            bessiecho = transform.GetChild(0).GetComponent<AudioSource>();

            bethanycolvin();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void bethanycolvin()
    {
        
        AudioListener.volume = PlayerPrefs.GetInt("MusicSetting", 1);
    }

    public void winifredmarrero()
    {
        AudioListener.volume = AudioListener.volume == 1 ? 0 : 1;

        PlayerPrefs.SetInt("MusicSetting", (int)AudioListener.volume);
        PlayerPrefs.Save();
    }

    public void savannahsalas(AudioClip clip)
    {
        bessiecho.PlayOneShot(clip);
    }
}
