using GleyMobileAds;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.SimpleSpinner
{
    [RequireComponent(typeof(Image))]
    public class dickflores : MonoBehaviour
    {
        [Header("Rotation")]
        public bool melvasantiago = true;
        [Range(-10, 10), Tooltip("Value in Hz (revolutions per second).")]
        public float latanyaclifton = 1;
        public AnimationCurve RotationAnimationCurve = AnimationCurve.Linear(0, 0, 1, 1);

        [Header("Rainbow")]
        public bool beryldaly = true;
        [Range(-10, 10), Tooltip("Value in Hz (revolutions per second).")]
        public float veralove = 0.5f;
        [Range(0, 1)]
        public float catalinawright = 1f;
        public AnimationCurve RainbowAnimationCurve = AnimationCurve.Linear(0, 0, 1, 1);

        [Header("Options")]
        public static bool RandomPeriod = true;
        
        private static Image _image;
        private static float _period;

        public void Start()
        {
            _image = GetComponent<Image>();
          _image.enabled = false;
            _period = RandomPeriod ? Random.Range(0f, 1f) : 0;
        }
        public static void StartSpin()
        {
            Debug.Log("StartSpin");
            if (_image.IsDestroyed())
            {
                return;
            }
            _image.enabled = true;
           
        }
        public static void Stop()
        {
            if (_image.IsDestroyed())
            {
                return;
            }
            Debug.Log("StoptSpin");

            _image.enabled = false;
        }

        public void Update()
        {
            
            
            
            

            
            
            
            
        }
    }
}
