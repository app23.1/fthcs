using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class quentinritter : MonoBehaviour
{
    private static string erikavillanueva = "URL_PREFIX";

    public InputField urlPrefixInput;
    public Text sdkVersionText;

    private string dorethaflaherty;

    
    public static void revatovar()
    {
        string prefix = PlayerPrefs.GetString(erikavillanueva, "");
        AudienceNetwork.AdSettings.SetUrlPrefix(prefix);
    }

    void Start()
    {
        dorethaflaherty = PlayerPrefs.GetString(erikavillanueva, "");
        urlPrefixInput.text = dorethaflaherty;
        sdkVersionText.text = AudienceNetwork.SdkVersion.Build;
    }

    public void OnEditEnd(string prefix)
    {
        dorethaflaherty = prefix;
        SaveSettings();
    }

    public void SaveSettings()
    {
        PlayerPrefs.SetString(erikavillanueva, dorethaflaherty);
        AudienceNetwork.AdSettings.SetUrlPrefix(dorethaflaherty);
    }

    public void staceyshapiro()
    {
        SceneManager.LoadScene("AdViewScene");
    }

    public void garrybaca()
    {
        SceneManager.LoadScene("InterstitialAdScene");
    }

    public void carsonbest()
    {
        SceneManager.LoadScene("RewardedVideoAdScene");
    }
}
